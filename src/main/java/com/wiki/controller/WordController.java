package com.wiki.controller;

import com.wiki.mapper.WordMapper;
import com.wiki.pojo.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("words")
public class WordController {

    @Autowired
    private WordMapper wordMapper;

    @GetMapping("getword")
    public HashMap<String, Object> getWord(HttpServletRequest req) {
        //        --- 接收 ---
        HashMap<String, Object> query = new HashMap<>();
        Enumeration<String> names = req.getParameterNames();
        while (names.hasMoreElements()) {
            String name = (String) names.nextElement();
            query.put(name, req.getParameter(name));
        }
        //        --- 处理 ---
        List<Word> words = wordMapper.getWords(query);
        //         --- 返回 ---
        HashMap<String, Object> result = new HashMap<>();
        result.put("code", 1);
        result.put("message", "success");
        result.put("data", words);
        return result;
    }

    @PostMapping("addword")
    public HashMap<String, Object> addWord(HttpServletRequest req) {
        //        --- 接收 ---
        HashMap<String, Object> params = new HashMap<>();
        Enumeration<String> names = req.getParameterNames();
        while (names.hasMoreElements()) {
            String name = (String) names.nextElement();
            params.put(name, req.getParameter(name));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        Date date = new Date();
        String now = simpleDateFormat.format(date);
        params.put("createTime", now);
        //        --- 处理 ---
        wordMapper.addWord(params);
        //         --- 返回 ---
        HashMap<String, Object> result = new HashMap<>();
        result.put("code", 1);
        result.put("message", "success");
        return result;
    }

    @PostMapping("delword")
    public HashMap<String, Object> delWord(HttpServletRequest req) {
        //        --- 接收 ---
        Integer id = Integer.parseInt(req.getParameter("id"));
        //        --- 处理 ---
        wordMapper.delWord(id);
        //         --- 返回 ---
        HashMap<String, Object> result = new HashMap<>();
        result.put("code", 1);
        result.put("message", "success");
        return result;
    }

    @PostMapping("updateword")
    public HashMap<String, Object> updateWord(HttpServletRequest req) {
        //      --- 接收 ---
        HashMap<String, Object> params = new HashMap<>();
        Enumeration<String> names = req.getParameterNames();
        while (names.hasMoreElements()) {
            String name = (String) names.nextElement();
            params.put(name, req.getParameter(name));
        }
        //        --- 处理 ---
        wordMapper.updateWord(params);
        //         --- 返回 ---
        HashMap<String, Object> result = new HashMap<>();
        result.put("code", 1);
        result.put("message", "success");
        return result;
    }
}

package com.wiki.mapper;

import com.wiki.pojo.Word;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Mapper
@Repository
public interface WordMapper {
    /**
     * 获取词条
     *
     * @param info 查询信息
     * @return 查询集
     */
    List<Word> getWords(@Param("info") HashMap<String, Object> info);

    /**
     * 添加词条
     *
     * @param info 词条内容
     */
    void addWord(@Param("info") HashMap<String, Object> info);

    /**
     * 修改词条
     *
     * @param info 包含id  按照id修改
     */
    void updateWord(@Param("info") HashMap<String, Object> info);

    /**
     * 删除词条
     *
     * @param id 需要删除词条的id
     */
    void delWord(@Param("id") Integer id);
}
